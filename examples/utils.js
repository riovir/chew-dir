/* eslint-disable no-console */

module.exports.print = print;
module.exports.run = run;

async function run(example) {
  printTitle(example.name);
  try { return await example(); }
  catch (err) { console.error('Error while running example', err); }
}

function print(mapper) {
  return file => { console.log(mapper(file)); };
}

function printTitle(title) {
  console.log(`\n------| ${title} |-----`);
}
