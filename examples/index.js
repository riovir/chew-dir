/* eslint-disable no-console */
const { prop, propEq } = require('ramda');
const { run, print } = require('./utils');

const { chewDir } = require('../lib');

runAll();

async function runAll() {
  await run(printByDefault);
  await run(cleanCopyAllFiles);
  await run(copySvgsAsLowerCase);
  await run(printFileNames);
  await run(findAllDirectories);
  await run(reduceFiles);
}

/** By default simply prints files in the path it was executed from */
function printByDefault() {
  return chewDir();
}

function printFileNames() {
  return chewDir({
    source: 'examples/source',
    process: print(prop('name'))
  });
}

function findAllDirectories() {
  return chewDir({
    source: 'examples/source',
    deep: true,
    includeDirectories: true,
    include: propEq('isDirectory', true),
    process: print(prop('name'))
  });
}

/**
 * API.copy uses fs-extra.copy under the hood, additionally resolving relative paths as well:
 * See: https://github.com/jprichardson/node-fs-extra/blob/master/docs/copy.md
 */
function copySvgsAsLowerCase() {
  return chewDir({
    source: 'examples/source',
    target: 'examples/target',
    deep: true,
    include: /\.svg$/i,
    process: async ({ absolutePath, base }, api) => {
      await api.copy(absolutePath, base.toLowerCase());
      console.log(`Copied ${base} as ${base.toLowerCase()}`);
    }
  });
}

/**
 * Note that API.copy also takes the file object for convenience.
 * In this case it will be copied to the target as is.
 */
function cleanCopyAllFiles() {
  return chewDir({
    source: 'examples/source',
    target: 'examples/target',
    includeDirectories: true,
    deep: true,
    cleanTarget: true,
    processAll: async (files, api) => files.forEach(api.copy)
  });
}

/**
 * All together! Using processAll to list files with unconventional names to a list.
 * API.outputFile uses fs-extra.outputFile under the hood, additionally resolving relative paths as well:
 * See: https://github.com/jprichardson/node-fs-extra/blob/master/docs/outputFile.md
 */
function reduceFiles() {
  chewDir({
    source: 'examples/source',
    target: 'examples/target',
    includeDirectories: true,
    deep: true,
    include: [/[A-Z]+/, hasSpace, '(', ')'],
    exclude: 'README.md',
    processAll: async (files, api) => {
      const content = files
        .map(file => file.base)
        .reduce(appendAsBulletPoint, '# Weird names');
      console.log('funky-names.md');
      console.log(content);
      return api.outputFile('funky-names.md', content);
    }
  });

  function hasSpace(file) {
    return file.name.includes(' ');
  }

  function appendAsBulletPoint(content, baseName) {
    return `${content}\n  - ${baseName}`;
  }
}
