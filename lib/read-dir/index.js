const { always, anyPass, either, filter, pipe, propEq, reject } = require('ramda');
const _addDetails = require('./add-details');
const _collectFiles = require('./collect-files');

module.exports = ReadDir();
module.exports.ReadDir = ReadDir;

function ReadDir({
  addDetails = _addDetails,
  collectFiles = _collectFiles
} = {}) {
  return async function readDir({
    fs,
    rootDir = '.',
    deep = false,
    includeDirectories = false,
    include = /.*/,
    exclude = []
  }) {
    const collection = [];
    const collectFrom = collectFiles({ fs, addDetails, collection, deep });
    const filterCollection = pipe(
      filter(isFileUnless(includeDirectories)),
      filter(matchedBy(include)),
      reject(matchedBy(exclude)));

    await collectFrom({ rootDir });
    return filterCollection(collection);
  };
}

function isFileUnless(include) {
  return either(always(include), propEq(false, 'isDirectory'));
}

function matchedBy(filter) {
  return Array.isArray(filter) ?
    anyPass(filter.map(matches)) :
    matches(filter);
}

function matches(filter) {
  if (typeof filter === 'function') { return filter; }
  if (typeof filter === 'string') {
    return ({ relativePath }) => relativePath.includes(filter);
  }
  if (filter.test) {
    return ({ relativePath }) => filter.test(relativePath);
  }
  throw new Error(`Unknown filter type. Should be one of [string, regex, predicate function]. Was ${filter}`);
}
