const _upath = require('upath');
const { andThen, converge, dropLast, identity, mergeRight, pick, pipe, pipeWith } = require('ramda');

module.exports = AddDetails();
module.exports.AddDetails = AddDetails;

function AddDetails({ upath = _upath } = {}) {
  const pipeP = pipeWith(andThen);
  return function addDetails({ fs, rootDir, relativeParent, processChildren }) {
    return pipeP([
      pathDetails(upath, { rootDir, relativeParent }),
      thenMergeWith(pathStats(fs)),
      thenMergeWith(contents(fs)),
      thenTap(processAnyChildren({ rootDir, processChildren }))
    ]);
  };
}

function pathDetails(upath, { rootDir, relativeParent }) {
  const parse = pipe(upath.parse, pick(['base', 'ext', 'name']));
  return async name => {
    const relativePath = relativeParent ? relativeParent + upath.sep + name : name;
    const absolutePath = upath.resolve(rootDir, relativePath);
    const parsed = parse(absolutePath);
    const relativeName = dropLast(parsed.ext.length, relativePath);
    return Object.assign(parsed, { absolutePath, relativePath, relativeName, relativeParent });
  };
}

function pathStats({ stat }) {
  return async ({ absolutePath }) => {
    const stats = await stat(absolutePath);
    return { stats, isDirectory: stats.isDirectory(), isFile: stats.isFile() };
  };
}

function contents({ readFile, createReadStream }) {
  return async ({ absolutePath, isFile }) => {
    const read = (opts = 'UTF-8', ...rest) => readFile(absolutePath, opts, ...rest);
    const readStream = (...opts) => createReadStream(absolutePath, ...opts);
    return isFile ? { read, readStream } : null;
  };
}

function processAnyChildren({ rootDir, processChildren }) {
  return async ({ relativePath, isDirectory }) => {
    return isDirectory && await processChildren({ rootDir, relativeParent: relativePath });
  };
}

function thenMergeWith(mapper) {
  const thenMerge = async (a, b) => mergeRight(await a, await b);
  return converge(thenMerge, [identity, mapper]);
}

function thenTap(consumer) {
  return async v => { await consumer(v); return v; };
}
