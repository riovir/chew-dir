const _upath = require('upath');
const { always } = require('ramda');

module.exports = CollectFiles();
module.exports.CollectFiles = CollectFiles;

function CollectFiles({ upath = _upath } = {}) {
  return function collectFiles({ fs, addDetails, collection, deep }) {
    return async function collectFrom({ rootDir, relativeParent = '' }) {
      const dir = upath.resolve(rootDir, relativeParent);
      const files = await fs.readdir(dir);
      const processChildren = deep ? collectFrom : always(Promise.resolve());
      const toDetailed = addDetails({ fs, rootDir, relativeParent, processChildren });
      const detailedFiles = await Promise.all(files.map(toDetailed));
      collection.push(...detailedFiles);
    };
  };
}
