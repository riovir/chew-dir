const { test } = require('tap');
const upath = require('upath');
const { always, join } = require('ramda');
const { AddDetails } = require('./add-details');

const ROOT_DIR = '/root';
const PARENT_DIR = 'subdir';
const FILE_EXT = '.md';
const FILE_NAME = 'test-file';
const FILE = FILE_NAME + FILE_EXT;
const DIRECTORY = 'test-directory';
const DATA = {
  '/root/subdir/test-file.md': { stats: { valid: true, isDirectory: always(false), isFile: always(true) } },
  '/root/subdir/test-directory': { stats: { valid: true, isDirectory: always(true), isFile: always(false) } }
};

test('always returns object with', async t => {
  const { result } = await detailsOf(FILE);

  t.equal(result.base, FILE, 'base prop');
  t.equal(result.ext, FILE_EXT, 'ext prop');
  t.equal(result.name, FILE_NAME, 'name prop');
  t.equal(result.absolutePath, `${ROOT_DIR}/${PARENT_DIR}/${FILE}`, 'absolutePath prop');
  t.equal(result.relativeName, `${PARENT_DIR}/${FILE_NAME}`, 'relativeName prop');
  t.equal(result.relativePath, `${PARENT_DIR}/${FILE}`, 'relativePath prop');
  t.equal(result.relativeParent, PARENT_DIR, 'relativeParent prop');
  t.equal(result.stats.valid, true, 'stats prop');
});

test('for files returns object with', async t => {
  const { result, verify } = await detailsOf(FILE);

  t.equal(result.isDirectory, false, 'false isDirectory prop');
  t.equal(result.isFile, true, 'true isFile prop');

  t.type(result.read, Function, 'read function');
  t.type(result.readStream, Function, 'readStream function');

  t.equal(verify.processChildren.length, 0, 'processChildren not invoked');
});

test('read()', async t => {
  const { result, verify } = await detailsOf(FILE);
  const calls = verify.readFile;
  result.read();
  t.equal(calls.length, 1, 'delegates to fs.readFile');
  t.equal(calls[0][0], `${ROOT_DIR}/${PARENT_DIR}/${FILE}`, 'calls fs.readFile with absolute path');
  t.equal(calls[0][1], 'UTF-8', 'sets UTF-8 encoding by default');
  result.read('NOT-UTF-8', 'other');
  t.equal(calls[1][1], 'NOT-UTF-8', 'allows overriding encoding');
  t.equal(calls[1][2], 'other', 'passes additional args along');
});

test('readStream()', async t => {
  const { result, verify } = await detailsOf(FILE);
  const calls = verify.createReadStream;
  result.readStream();
  t.equal(calls.length, 1, 'delegates to fs.createReadStream');
  t.equal(calls[0][0], `${ROOT_DIR}/${PARENT_DIR}/${FILE}`, 'calls fs.readFile with absolute path');
  result.readStream('some', 'args');
  t.equal(calls[1][2], 'args', 'passes additional args along');
});

test('returns object with', async t => {
  const { result, verify } = await detailsOf(DIRECTORY);

  t.equal(result.isDirectory, true, 'true isDirectory prop');
  t.equal(result.isFile, false, 'false isFile prop');

  t.notOk(result.read, 'no isDirectory function');
  t.notOk(result.readStream, 'no isFile function');

  t.equal(verify.processChildren.length, 1, 'processChildren invoked');
});

async function detailsOf(path) {
  const mockStat = path => DATA[path].stats;
  const verify = { readFile: [], createReadStream: [], processChildren:[] };
  const mockFs = {
    stat: mockStat,
    verify,
    readFile: (...args) => verify.readFile.push(args),
    createReadStream: (...args) => verify.createReadStream.push(args)
  };
  const mockResolve = (...args) => join('/', args);
  const mockUpath = new Proxy(upath, {
    get: (obj, prop) => prop === 'resolve' ? mockResolve : obj[prop]
  });
  const addDetails = AddDetails({ upath: mockUpath });
  const result = await addDetails({
    fs: mockFs,
    rootDir: ROOT_DIR,
    relativeParent: PARENT_DIR,
    processChildren: (...args) => verify.processChildren.push(args)
  })(path);
  return { result, verify };
}
