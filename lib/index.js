const _upath = require('upath');
const _fsRetry = require('./fs-retry');
const readDir = require('./read-dir');
const ProcessApi = require('./process-api');
const { __, includes, propSatisfies, unless } = require('ramda');

module.exports.chewDir = ChewDir();

function ChewDir({
  fsRetry = _fsRetry,
  root = process.cwd(),
  upath = _upath
} = {}) {
  const resolvePath = unless(upath.isAbsolute, relative => upath.resolve(root, relative));

  return async function chewDir({
    source = '.',
    target = '.',
    deep = false,
    cleanTarget = false,
    includeDirectories = false,
    include = /.*/,
    exclude = [],
    process = (file, _api) => file,
    processAll = (files, api) => mapAsync(file => process(file, api), files),
    retries = 3,
    retryInterval = 50,
    ignoreWhen = errorIs('EEXIST'),
    retryWhen = errorIs('EBUSY', 'ENOENT')
  } = { processAll: logFiles }) {
    const rootDir = resolvePath(source);
    const fs = fsRetry({ retries, retryInterval, ignoreWhen, retryWhen });
    const files = await readDir({ fs, rootDir, deep, includeDirectories, include, exclude });

    const targetPath = resolvePath(target);
    if (cleanTarget) { await fs.emptyDir(targetPath); }

    const api = ProcessApi({ fs, sourcePath: rootDir, targetPath });
    return processAll(files, api);
  };
}

async function mapAsync(mapper, array) {
  return Promise.all(array.map(mapper));
}

function errorIs(...codes) {
  return propSatisfies(includes(__, codes), 'code');
}

/* eslint-disable */
async function logFiles(files) {
  files.forEach(file => console.log(file));
  return files;
}
/* eslint-enable */
