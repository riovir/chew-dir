const _upath = require('upath');

module.exports = ProcessApi();
module.exports.ProcessApi = ProcessApi;

function ProcessApi({ upath = _upath } = {}) {
  return function processApi({ fs, sourcePath, targetPath }) {
    return {
      copy: copy({ fs, upath, sourcePath, targetPath }),
      outputFile: outputFile({ fs, upath, targetPath })
    };
  };
}

function outputFile({ fs, upath, targetPath }) {
  return async function outputFile(file, data, opts = 'UTF-8', ...rest) {
    const path = upath.resolve(targetPath, file);
    return fs.outputFile(path, data, opts, ...rest);
  };
}

function copy({ fs, upath, sourcePath, targetPath }) {
  const tryCopy = fsCopy({ fs, upath, sourcePath, targetPath });
  return async function copy(src, dest, ...rest) {
    return src.relativePath ?
      tryCopy(src.relativePath, null, { overwrite: true, errorOnExist: false }) :
      tryCopy(src, dest, ...rest);
  };
}

function fsCopy({ fs, upath, sourcePath, targetPath }) {
  return (src, dest, ...rest) => {
    const source = upath.resolve(sourcePath, src);
    const target = upath.resolve(targetPath, dest || src);
    return fs.copy(source, target, ...rest);
  };
}
