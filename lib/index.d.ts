import { Stats, outputFile, copy } from 'fs-extra';

/**
 * Processes files in a source dir either all together or one by one.
 *
 * If invoked without any options it will console.log all files found in the current dir.
 * */
export function chewDir<T>(options?: ChewOptions<T>): Promise<T>;

type ChewOptions<T> = {
  /** Source dir to process. Uses 'process.cwd()' as basis for relative path. */
  source?: string,
  /** Target dir used by the convenience API. Uses 'process.cwd()' as basis for relative path. */
  target?: string,
  /** Enable deep (recursive) traversal of 'source'. False by default. */
  deep?: boolean,
  /** Empty target first. False by default. */
  cleanTarget?: boolean,
  /** Include dirs in processed files. False by default. */
  includeDirectories?: boolean,
  /** Only include results matched by the pattern(s). String and RegExp uses 'relativeName' of results. */
  include?: Pattern | Pattern[],
  /** Exclude results matched by the pattern(s). String and RegExp uses 'relativeName' of results. Takes precedence over 'include'. */
  exclude?: Pattern | Pattern[],
  /** Process files all at once. Takes precedence over process. */
  processAll?: (files: ChewFile[], api: ProcessApi) => T | Promise<T> | void,
  /** Process files one by one. */
  process?: (file: ChewFile, api: ProcessApi) => T | Promise<T> | void,
  /** Amount of retries on common temporary file system errors. 3 by default.*/
  retries?: number,
  /** Milliseconds to wait between retries. 50 by default. */
  retryInterval?: number,
  /** Error predicate to check if failed fs operation should be retried. Matches 'EBUSY' and 'ENOENT' codes by default. */
  retryWhen?: (err: Error) => boolean
  /** Error predicate to check what to ignore. Matches 'EEXIST' code by default. */
  ignoreWhen?: any,
};

type ChewFile = {
  /** Base file or dir name. Eg.: "package.json" */
  base: string,
  /** Extension of a file or empty string for dir. Eg.: ".json" */
  ext: string,
  /** Name of a file or dir. Eg.: "package" */
  name: string,
  /** Absolute path in POSIX format. Eg.: "d:/workspaces/chew-dir/package.json" */
  absolutePath: string,
  /** Path relative to the 'source' option in POSIX format. Eg.: "chew-dir/package.json" */
  relativePath: string,
  /** Name relative to the 'source' option in POSIX format. Eg.: "chew-dir/package" */
  relativeName: string,
  /** Path to parent dir relative to the 'source' option in POSIX format. Eg.: "chew-dir" */
  relativeParent: string,
  isDirectory: boolean,
  isFile: boolean,
  /** Stats returned by fs-extra */
  stats: Stats,
  /** Read contents using fs-extra readFile with UTF-8 encoding. Only for files. */
  read?: () => Promise<string>,
  /** Creates read stream using fs-extra createReadStream. Only for files. */
  readStream?: () => Promise<string>,
};

type ProcessApi = {
  /** Same as fs-extra copy that can also be called with a single file argument. */
  copy: typeof copy | ((src: { relativePath }) => Promise<void>),
  /** Same as fs-extra outputFile with encoding set to UTF-8 by default. */
  outputFile: typeof outputFile,
};

type Pattern = string | RegExp | ((element: ChewFile) => boolean);
