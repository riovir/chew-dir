const { test } = require('tap');
const { propEq } = require('ramda');
const upath = require('upath');
const fsRetry = require('./fs-retry');
const readDir = require('./read-dir');

test('finds files', async t => {
  t.equal((await read()).length, 5, 'in source root');
  t.equal((await read({ deep: true })).length, 12, 'deep in source root');
});

test('finds directories', async t => {
  t.equal((await read({
    includeDirectories: true,
    include: propEq(true, 'isDirectory')
  })).length, 2, 'in source root');

  t.equal((await read({
    deep: true,
    includeDirectories: true,
    exclude: propEq(true, 'isFile')
  })).length, 3, 'deep in source root');
});

test('supports reading file contents', async t => {
  const svg = await read({ include: 'icon-one.svg' });
  const contents = await svg[0].read();
  t.match(contents, 'svg', 'supports reading file contents');
});

test('resolves supported filter types', async t => {
  await t.resolves(read({ include: 'some text' }));
  await t.resolves(read({ include: /regex/ }));
  await t.resolves(read({ include: file => !!file.ext }));
});

test('reject unsupported filter types', async t => {
  await t.rejects(read({ include: 5 }));
  await t.rejects(read({ exclude: {} }));
});

test('searches in local directory by default', async t => {
  const rootFiles = await read({ rootDir: undefined });
  t.ok(rootFiles.find(propEq('package.json', 'base')));
});

function read(opts) {
  const sourcePath = upath.resolve(__dirname, '../examples/source');
  return readDir(Object.assign({ fs: fsRetry(), rootDir: sourcePath }, opts));
}
