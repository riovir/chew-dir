const { test } = require('tap');
const { propEq } = require('ramda');
const { record } = require('../../test/utils');
const tryTimes = require('./try-times');

const ignoreWhen = propEq('ignore', 'message');
const retryWhen = propEq('retry', 'message');
const tryOnce = tryTimes({ retries: 1, retryInterval: 1, ignoreWhen, retryWhen });
const tryThreeTimes = tryTimes({ retries: 3, retryInterval: 1, ignoreWhen, retryWhen });

test('when no error occurs', async t => {
  const workWell = record(() => {});
  await t.resolves(tryThreeTimes(workWell), 'successfully executes attempt');
  t.equal(workWell.calls.length, 1, 'does not retries');
});

test('when retryWhen matches', async t => {
  const error = errorOf('retry');
  await t.rejects(tryThreeTimes(error), 'rejects attempt');
  t.equal(error.calls.length, 3, 'retries specified times');
});

test('when ignoreWhen matches', async t => {
  const error = errorOf('ignore');
  await t.resolves(tryThreeTimes(error), 'resolves attempt skipping it');
  t.equal(error.calls.length, 1, 'does not retry');
});

test('when times < 2', async t => {
  const error = errorOf('retry');
  await t.rejects(tryOnce(error), 'rejects attempt');
  t.equal(error.calls.length, 1, 'does not retries on error');
});

function errorOf(message) {
  return record(() => { throw new Error(message); });
}
