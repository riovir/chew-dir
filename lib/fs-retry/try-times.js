const { always, F, ifElse } = require('ramda');

module.exports = tryTimes;

function tryTimes({ retries, retryInterval, ignoreWhen, retryWhen }) {
  const canRetry = onlyWhen(retries > 1, retryWhen);
  const retry = attempt => tryTimes({ retries: retries - 1, retryInterval, ignoreWhen, retryWhen })(attempt);
  const again = snoozeThen({ retryInterval, retry });
  return tryTo({ ignoreWhen, canRetry, again });
}

function onlyWhen(check, filter) {
  return ifElse(always(check), filter, F);
}

function tryTo({ ignoreWhen, canRetry, again }) {
  return async attempt => {
    try { return await attempt(); }
    catch (err) {
      const reattempt = () => again(attempt);
      const handle = handleError({ ignoreWhen, canRetry, reattempt });
      await handle(err);
    }
  };
}

function handleError({ ignoreWhen, canRetry, reattempt }) {
  return err => {
    if (ignoreWhen(err)) { return; }
    if (canRetry(err)) { return reattempt(); }
    throw err;
  };
}

function snoozeThen({ retryInterval, retry }) {
  return async attempt => {
    await new Promise(resolve => {
      setTimeout(resolve, retryInterval);
    });
    return await retry(attempt);
  };
}
