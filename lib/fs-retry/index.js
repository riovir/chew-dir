const _fs = require('fs-extra');
const { __, includes, is, propSatisfies, when } = require('ramda');
const tryTimes = require('./try-times');

module.exports = FsRetry();
module.exports.FsRetry = FsRetry;

function FsRetry({ fs = _fs } = {}) {
  return function fsRetry({
    retries = 3,
    retryInterval = 50,
    ignoreWhen = errorIs('EEXIST'),
    retryWhen = errorIs('EBUSY', 'ENOENT'),
  } = {}) {
    const retryIt = tryTimes({ retries, retryInterval, ignoreWhen, retryWhen });
    return new Proxy(fs, { get: handleWith(retryIt) });
  };
}

function errorIs(...codes) {
  return propSatisfies(includes(__, codes), 'code');
}

function handleWith(wrapper) {
  const wrapFunctions = when(is(Function), wrapWith(wrapper));
  return (obj, prop) => wrapFunctions(obj[prop]);
}

function wrapWith(wrapper) {
  return func => (...args) => {
    return wrapper(() => func(...args));
  };
}
