const { test } = require('tap');
const { record } = require('../test/utils');
const { FsRetry } = require('./fs-retry');

test('when no error occurs', async t => {
  const { fs, doOk } = fsRetry({ retries: 2, retryInterval: 200 });
  await t.resolves(fs.doOk(), 'successfully executes attempt');
  t.equal(doOk.calls.length, 1, 'does not retry');
});

test('on EEXIST error code', async t => {
  const { fs, throwError } = fsRetry({ retries: 5 });
  await t.resolves(fs.throwError('EEXIST'), 'resolves attempt skipping it');
  t.equal(throwError.calls.length, 1, 'does not retries');
});

test('on EBUSY error code', async t => {
  const { fs, throwError } = fsRetry({ retries: 5 });
  await t.rejects(fs.throwError('EBUSY'), 'rejects attempt');
  t.equal(throwError.calls.length, 5, 'retries specified times');
});

test('on ENOENT error code', async t => {
  const { fs, throwError }  = fsRetry({ retries: 5 });
  await t.rejects(fs.throwError('ENOENT'), 'rejects attempt');
  t.equal(throwError.calls.length, 5, 'retries specified times');
});

function fsRetry({ retries, retryInterval = 1, ignoreWhen, retryWhen }) {
  const mockFs = MockFs();
  const fs = FsRetry({ fs: mockFs })({ retries, retryInterval, ignoreWhen, retryWhen });
  return Object.assign({ fs }, mockFs);
}

function MockFs() {
  const err = new Error('Mock fs error');
  return {
    doOk: record(async () => 'OK'),
    throwError: record(async code => Promise.reject(Object.assign({}, err, { code })))
  };
}
