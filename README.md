# chew-dir [![pipeline status](https://gitlab.com/riovir/chew-dir/badges/master/pipeline.svg)](https://gitlab.com/riovir/chew-dir/commits/master)[![coverage report](https://gitlab.com/riovir/chew-dir/badges/master/coverage.svg)](https://gitlab.com/riovir/chew-dir/commits/master)

> Chew through a directory processing files as you go!

_Note that this package is inteded to be used for ad-hoc scripts churning through a directory worth of files converting them to some other form. For your production needs you might want to look elsewhere._

## Usage

* **All options have a default.** Prints files from the working directory to the console, unless either the `process` or `processAll` function is specified.
* Supports async behavior returning a `Promise` where applicable.
* You'll find further examples in the [/examples](https://gitlab.com/riovir/chew-dir/blob/master/examples/index.js#L59) directory.

### Example 

Deep scan directory for all SVGs and copy them to target flattened and lower cased.

```JavaScript
await chewDir({
  source: 'examples/source',
  target: 'examples/target',
  deep: true,
  include: /\.svg$/i,
  process: async ({ absolutePath, base }, api) => {
    await api.copy(absolutePath, base.toLowerCase());
    console.log(`Copied ${base} as ${base.toLowerCase()}`);
  }
});
```

## API

See [type definitions](https://gitlab.com/riovir/chew-dir/blob/master/lib/index.d.ts) for docs.

```TypeScript
type ChewOptions<T> = {
  /** Source dir to process. Uses 'process.cwd()' as root for relative path. */
  source?: string,
  /** Target dir used by the convenience API. Uses 'process.cwd()' as root for relative path. */
  target?: string,
  /** Enable deep (recursive) traversal of 'source'. False by default. */
  deep?: boolean,
  /** Empty target first. False by default. */
  cleanTarget?: boolean,
  /** Include dirs in processed files. False by default. */
  includeDirectories?: boolean,
  /** Only include results matched by the pattern(s). String and RegExp uses 'relativeName' of results. */
  include?: Pattern | Pattern[],
  /** Exclude results matched by the pattern(s). String and RegExp uses 'relativeName' of results. Takes precedence over 'include'. */
  exclude?: Pattern | Pattern[],
    /** Process files all at once. Takes precedence over process */
	processAll?: (files: ChewFile[], api: ProcessApi) => T | Promise<T> | void,
  /** Process files one by one. */
  process?: (file: ChewFile, api: ProcessApi) => T | Promise<T> | void,
  /** Set amount of retries on common temporary file system errors. 3 by default.*/
  retries?: number,
  /** Set milliseconds to wait between retries. 50 by default. */
  retryInterval?: number,
  /** Error predicate to check if failed fs operation should be retried. Matches 'EBUSY', 'ENOENT' by default. */
  retryWhen?: (err: Error) => boolean
  /** Error predicate to check what to ignore. Matches 'EEXIST' by default. */
  ignoreWhen?: any,
};

type ChewFile = {
  /** Base file or dir name. Eg.: "package.json" */
  base: string,
  /** Extension of a file or empty string for dir. Eg.: ".json" */
  ext: string,
  /** Name of a file or dir. Eg.: "package" */
  name: string,
  /** Absolute path in POSIX format. Eg.: "d:/workspaces/chew-dir/package.json" */
  absolutePath: string,
  /** Path relative to the 'source' option in POSIX format. Eg.: "chew-dir/package.json" */
  relativePath: string,
  /** Name relative to the 'source' option in POSIX format. Eg.: "chew-dir/package" */
  relativeName: string,
  /** Path to parent dir relative to the 'source' option in POSIX format. Eg.: "chew-dir" */
  relativeParent: string,
  isDirectory: boolean,
  isFile: boolean,
  /** Stats returned by fs-extra */
  stats: Stats,
  /** Read contents using fs-extra readFile with UTF-8 encoding. Only for files. */
	read?: () => Promise<string>,
  /** Creates read stream using fs-extra createReadStream. Only for files. */
	readStream?: () => Promise<string>,
};

type ProcessApi = {
  /** Same as fs-extra copy that can also be called with a single file argument. */
  copy: typeof copy,
  /** Same as fs-extra outputFile with encoding set to UTF-8 by default. */
  outputFile: typeof outputFile,
};

type Pattern = string | RegExp | ((el: ChewFile) => boolean);
```
