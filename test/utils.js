module.exports.record = record;

function record(func) {
  const calls = [];
  function recordedFunction(...args) {
    calls.push(args);
    return func(...args);
  }
  return Object.assign(recordedFunction, { calls });
}
