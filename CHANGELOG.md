# Change Log
All notable changes to this project will be documented in this file.

> Before releasing 1.0.0 API breaking changes will increase `minor` version. Everything else is `patch`.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.2] - 2023-07-31
### Fixed
- Outdated dependencies

## [0.2.1] - 2021-06-21
### Fixed
- Outdated dependencies.

## [0.2.0] - 2021-03-30
### Changed
- Minimum Node version has been raised to **12**.
- The default export is now exported named as `chewDir`.
- Named export `ChewDir` factory function is no longer exported.
- Option `process` is only used if `processAll` is not set, included in its default implementation.

### Added
- Added type definitions for better TypeScript and dev experience

### Fixed
- Outdated dependencies.

## [0.1.2] - 2020-11-11
### Fixed
- Outdated dependencies.

## [0.1.1] - 2020-09-08
### Fixed
- Outdated and vulnerable dependencies.

## [0.1.0] - 2020-03-23
### Fixed
- Outdated and vulnerable dependencies.

## [0.0.6] - 2018-06-20
- **defaultProcess** function also returns the file it receives.

## [0.0.5] - 2018-05-07
- **File object** introduce prop `relativeName` which is the `relativePath` without the `ext`

## [0.0.4] - 2018-05-06
- **main.js** relocated to `lib/index.js`

## [0.0.3] - 2018-05-06
- **Remove unnecessary files** from published package

## [0.0.1] - 2018-05-06
- **Initial release. See README for details**

